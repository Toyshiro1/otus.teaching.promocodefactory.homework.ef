﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _repository;
        public CustomersController(IRepository<Customer> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns>Список Dto клиентов</returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var result = await _repository.GetAllAsync();
            return Ok(result.Select(c => new CustomerShortResponse
            {
                Id = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                Email = c.Email,
                preferences = c.CustomerPreferences
                .Select(p => new PrefernceResponse { Id = p.Preference.Id, Name = p.Preference.Name })
                .ToArray()
            }));
        }
        
        /// <summary>
        /// Получение клиента по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns>Дто клиента</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var result = await _repository.GetByIdAsync(id);
            if (result == null)
                throw new Exception("Не удалось получить клиента");
            return Ok(result);
        }
        
        /// <summary>
        /// Создание клиента
        /// </summary>
        /// <param name="request">Dto с данными создания клиента</param>
        /// <returns>Id созданого клиента</returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var id = await _repository.Add(new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            });

            return Ok(id);
        }
        
        /// <summary>
        /// Обновление данных клиента
        /// </summary>
        /// <param name="id">Идентификатор обновляеиого клиента</param>
        /// <param name="request">Дто с данными для обновления</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            await _repository.Update(new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            });
            return Ok();
        }
        
        /// <summary>
        /// Удаляет клиента
        /// </summary>
        /// <param name="id">Идентификатор удаляемого клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            await _repository.Remove(id);
            return Ok();
        }
    }
}