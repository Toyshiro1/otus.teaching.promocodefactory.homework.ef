﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private IRepository<Preference> _repository;
        public PreferenceController(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получение списка предпочтений
        /// </summary>
        /// <returns>Dto предпочтения</returns>
        [HttpGet]
        public async Task<ActionResult<PrefernceResponse>> GetCustomersAsync()
        {
            var result = await _repository.GetAllAsync();
            return Ok(result.Select(c => new PrefernceResponse
            {
                Id = c.Id,
                Name = c.Name
            }));
        }
    }
}
