﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Xml.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private DataContext _dataContext;

        public EfRepository(DataContext context)
        {
            _dataContext = context;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(_dataContext.Set<T>().AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(_dataContext.Set<T>().FirstOrDefault(e => e.Id == id));
        }

        public Task<Guid> Add(T entity)
        {
            var id = _dataContext.Set<T>().Add(entity).Entity.Id;
            _dataContext.SaveChanges();
            return Task.FromResult(id);
        }

        public async Task Update(T entity)
        {
            var eDb = _dataContext.Set<T>().Find(entity.Id);
            if (eDb != null)
            {
                _dataContext.Entry(eDb).CurrentValues.SetValues(entity);
                _dataContext.SaveChanges();
            }
            else
                throw new Exception("Запись не найдена");
        }

        public async Task Remove(Guid id)
        {
            var eDb = _dataContext.Set<T>().Find(id);
            _dataContext.Set<T>().Remove(eDb);
            _dataContext.SaveChanges();
        }
    }
}
