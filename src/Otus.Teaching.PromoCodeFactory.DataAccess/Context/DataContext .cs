﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
        : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        public DbSet<Employee> Employes { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasKey(r => r.Id);
            modelBuilder.Entity<Role>().Property(x => x.Name).HasMaxLength(20);
            modelBuilder.Entity<Role>().Property(x => x.Description).HasMaxLength(100);


            modelBuilder.Entity<Employee>().HasKey(r => r.Id);
            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role);

            modelBuilder.Entity<Employee>().Property(x => x.FirstName).HasMaxLength(20);
            modelBuilder.Entity<Employee>().Property(x => x.LastName).HasMaxLength(20);
            modelBuilder.Entity<Employee>().Ignore(x => x.FullName);
            modelBuilder.Entity<Employee>().Property(x => x.Email).HasMaxLength(20);


            modelBuilder.Entity<Customer>().HasKey(r => r.Id);
            //modelBuilder.Entity<Customer>()
            //    .HasMany(c => c.preferences);
            //.WithMany


            modelBuilder.Entity<Customer>().Property(x => x.FirstName).HasMaxLength(20);
            modelBuilder.Entity<Customer>().Property(x => x.LastName).HasMaxLength(20);
            modelBuilder.Entity<Customer>().Ignore(x => x.FullName);
            modelBuilder.Entity<Customer>().Property(x => x.Email).HasMaxLength(20);


            modelBuilder.Entity<Preference>().HasKey(r => r.Id);

            modelBuilder.Entity<CustomerPreference>().HasKey(sc => new { sc.CustomerId, sc.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(sc => sc.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(cp => cp.PreferenceId);

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<CustomerPreference>().HasData(new CustomerPreference 
            {
                CustomerId = FakeDataFactory.Customers.First().Id,
                PreferenceId = FakeDataFactory.Preferences.First().Id,
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=homework2.db");
            optionsBuilder
                .UseLazyLoadingProxies();

        }
    }
}
